#!/bin/bash
PAUSE=20
SHOTS_PER_MIN=$((60/$PAUSE))
echo "Hey, we're starting the screen logger!"
echo "We're going to take $SHOTS_PER_MIN shots per minute."
MAX_SHOTS=$(($SHOTS_PER_MIN*60*24))
echo "This script will continue for 24 hours unless stopped, up to $MAX_SHOTS shots will be taken."
echo "You can stop at any time by opening up this window and clicking Cancel. Feel free to hide the app while it's running."
echo "All output will be saved to a folder called screenshots on your Desktop - we'll create the folder for you if it doesn't exist."
echo "OK, let's go!"
echo "********"
TIMESTAMP=$(date +"%d-%m-%y %T")
COUNTER=0
mkdir ~/Desktop/screenshots
while [ $COUNTER -lt $MAX_SHOTS ]; do
	let COUNTER=COUNTER+1
	TIMESTAMP=$(date +"%y%m%d-%H.%M.%S")
	echo "Capturing shot #$COUNTER at $TIMESTAMP"
	CAPTURE_STR="screencapture -x -C ~/Desktop/screenshots/sc-$TIMESTAMP.png"
	eval $CAPTURE_STR
	echo "Waiting $PAUSE seconds..."
	sleep $PAUSE
done